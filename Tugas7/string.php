<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Soal String PHP</title>
</head>
<body>
    <h1>Soal String</h1>
    <?php
    //Soal Nomor 1
        echo "<h3>Soal String 1</h3>";
        $first_sentence = "Hello PHP!" ; 
        $second_sentence = "I'm ready for the challenges"; 

        echo "First sentence : " . $first_sentence . "<br>" ;
        echo "Panjang First sentence : " . strlen($first_sentence). "<br>" ;
        echo "Jumlah kata First sentence : " . str_word_count($first_sentence) . "<br>" . "<br>" ;

        echo "Second sentence : " . $second_sentence . "<br>" ;
        echo "Panjang Second sentence : " . strlen($second_sentence). "<br>" ;
        echo "Jumlah kata Second sentence : " . str_word_count($second_sentence) . "<br>" ;

    // Soal Nomor 2
        echo "<h3>Soal String 2</h3>";
        $string2 = "I love PHP";
        
        echo "<label>String: </label> \"$string2\" <br>";
        echo "Kata pertama: " . substr($string2, 0, 1) . "<br>" ; 
        echo "Kata kedua: " . substr($string2, 2, 5) . "<br>";
        echo "Kata Ketiga: " . substr($string2, 7) . "<br>";

    //Soal Nomor 3
        echo "<h3>Soal String 3</h3>";
         $string3 = "PHP is old but sexy!";

        echo "Kalimat String: " . $string3 . "<br>"; 
        echo "Kalimat ganti : " . str_replace ("sexy" , "awesome" , $string3) ;

    ?>
</body>
</html>