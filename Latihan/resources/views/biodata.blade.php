<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form method="post"action="/welcome">
        @csrf
        
        <label>First name:</label><br> <br>
        <input type="text" name="fname" required><br><br>

        <label >Last name:</label> <br><br>
        <input type="text" name="lname" required><br><br>

        <label >Gender:</label><br><br>
        <input type="radio" name="Gender" value="1">Male <br>
        <input type="radio" name="Gender" value="2">Female <br>
        <input type="radio" name="Gender" value="3">Other <br><br>

        <label >Nationality:</label> <br><br>
        <select name="Nationality" required>
            <option value="1">Indonesian</option>
            <option value="2">Malaysian</option>
            <option value="3">Singapura</option>
            <option value="4">other</option>
        </select> <br><br>

        <label >Language Spoken:</label> <br>
        <input type="checkbox"value="1"name="Language Spoken">Bahasa Indonesia <br>
        <input type="checkbox"value="2"name="Language Spoken">English <br>
        <input type="checkbox"value="3"name="Language Spoken">Other <br> <br>

        <label >Bio</label> <br>
        <textarea name="Bio" cols="20" rows="10" required></textarea><br><br>

        <input type="Submit"value="Sign Up">
    </form>
</body>
</html>