<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Loopping</title>
</head>
<body>
    <h1>Soal Loopping</h1>
    <?php
    //Soal Nomor 1

    //Looping I Love PHP
    //Lakukan Perulangan (boleh for/while/do while) sebanyak 20 iterasi. Looping terbagi menjadi dua: Looping yang pertama Ascending (meningkat) 
    //dan Looping yang ke dua menurun (Descending). 
        
        echo "<h3>Soal Nomor 1</h3>";
        echo "<h4>Loopping Pertama</h4>";
        for($i=2; $i<=20; $i+=2){
            echo $i . " - I Love PHP <br>";
        }
        echo "<h4>Loopping Kedua</h4>";
        for($i=20; $i>=2; $i-=2){
            echo $i . " - I Love PHP <br>";
        }

    //Soal Nomor 2
    /* 
    Looping Array Module
    Carilah sisa bagi dengan angka 5 dari setiap angka pada array berikut.
    Tampung ke dalam array baru bernama $rest 
    */
        echo "<h3>Soal Nomor 2</h3>";
        $numbers = [18, 45, 29, 61, 47, 34];
        echo "Array numbers: ";
        print_r($numbers);
        echo "<br>";
        echo "Array sisa baginya adalah:  "; 
        foreach($numbers as $value){
            $rest[] = $value %= 5;
        }
        print_r($rest);
        echo "<br>";

    //Soal Nomor 3
    /* Loop Associative Array 
        Terdapat data items dalam bentuk array dimensi. Buatlah data tersebut ke dalam bentuk Array Asosiatif. 
        Setiap item memiliki key yaitu : id, name, price, description, source. */

        echo "<h3>Soal Nomor 3</h3>";
        $barang = [
            [001 , "Keyboard Logitek " , 60000 ,  "Keyboard yang mantap untuk kantoran" ,  "logistek.jpeg" ],
            [002 , "Keyboard MSI " , 300000 ,  "Keyboard gaming MSI mekanik" ,  "msi.jpeg" ],
            [003 , "Mouse Genius " , 50000 , "Mouse Genius biar lebih pinter" ,  "genius.jpeg" ],
            [004 , "Mouse Jerry " , 30000 ,  "Mouse yang disukai kucing" ,  "jerry.jpeg" ],
        ];
        foreach($barang as $key => $value){
            $items = array(
                "ID" => $value[0],
                "Name" => $value[1],
                "Price" => $value[2],
                "Description" => $value[3],
                "Source" => $value[4],
            );

            print_r($items);
            echo "<br>";
        }

    //Soal Nomor 4
    /* Asterix 5x5
        Tampilkan dengan looping dan echo agar menghasilkan kumpulan bintang dengan pola seperti berikut: */

        echo "<h3>Soal Nomor 4</h3>";
        for ($j = 1; $j <= 5; $j++){
            for ($b =1; $b <= $j; $b++){
                echo "*" ;
            }
            echo "<br>";
        }

        


    ?>
    
</body>
</html>