<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Function</title>
</head>
<body>
    <h1>Soal Conditional and Function</h1>
    <?php
    //Soal Nomor 1
    //Buatlah sebuah function greetings() yang menerima satu parameter berupa string. 
        echo "<h3>Soal No 1 Greetings </h3>";
        function greetings ($nama){
            echo "Halo" . $nama . ", Selamat Datang di PKS Digital School <br>" ;
        }

        greetings("Bagas");
        greetings("Wahyu");
        greetings("Abdul");

        echo "<br>";

    //Soal Nomor 2
    /* Reverse String
    Buatlah sebuah function reverseString() untuk mengubah string berikut menjadi kebalikannya menggunakan function dan looping (for/while/do while).
    Function reverseString menerima satu parameter berupa string.
    NB: DILARANG menggunakan built-in function PHP sepert strrev(), HANYA gunakan LOOPING! */
        
        echo "<h3>Soal No 2 Reverse String </h3>"; 
        function reverse ($kata1){
            $katapanjang = strlen($kata1);
            $tampung = "";
            for ($i=($katapanjang-1); $i >= 0; $i --){
                $tampung .= $kata1[$i];
            }
            return $tampung;
        } 

        function reverseString ($kata2){
            $string=reverse ($kata2);
            echo $string ."<br>";
        }

        reverseString("nama peserta");
        reverseString("Sanbercode");
        reverseString("We Are Sanbers Developers");
        echo "<br>" ;

    //Soal Nomor 3
    /* Palindrome
    Buatlah sebuah function yang menerima parameter berupa string yang mengecek apakah string tersebut sebuah palindrome atau bukan. 
    Palindrome adalah sebuah kata atau kalimat yang jika dibalik akan memberikan kata yang sama contohnya: katak, civic.
    Jika string tersebut palindrome maka akan mengembalikan nilai true, sedangkan jika bukan palindrome akan mengembalikan false.
    NB: DILARANG menggunakan built-in function PHP seperti strrev() dll. Gunakan looping seperti biasa atau gunakan function reverseString dari jawaban no.2! */

        echo "<h3>Soal No 3 Palindrome </h3>";
        function palindrome ($pali){
            $balikKata = reverse ($pali);
            if($pali === $balikKata){
                echo "$pali => true <br>";
            }else {
               echo "$pali => false <br>";
            }
        }

        palindrome("civic") ; 
        palindrome("nababan") ; 
        palindrome("jambaban"); 
        palindrome("racecar"); 

    //Soal Nomor 4
    /* buatlah sebuah function bernama tentukan_nilai . Di dalam function tentukan_nilai yang menerima parameter 
    berupa integer. dengan ketentuan jika paramater integer lebih besar dari sama dengan 85 dan lebih kecil sama dengan 100 maka akan mereturn String “Sangat Baik” 
    Selain itu jika parameter integer lebih besar sama dengan 70 dan lebih kecil dari 85 maka akan mereturn string “Baik” selain itu jika parameter number lebih besar 
    sama dengan 60 dan lebih kecil dari 70 maka akan mereturn string “Cukup” selain itu maka akan mereturn string “Kurang” */

    echo "<h3>Soal No 4  </h3>";  
    function tentukan_nilai ($angka){
        if ($angka >= 85 && $angka < 100){
           return "$angka => Sangat Baik <br>";
        }else if ($angka >= 70 && $angka < 85){
            return "$angka => Baik <br>";
        }else if($angka >= 60 && $angka < 70){
            return "$angka => Cukup <br>";
        }else {
            return "$angka => Kurang <br>";
        }

    }

        echo tentukan_nilai(98); 
        echo tentukan_nilai(76); 
        echo tentukan_nilai(67); 
        echo tentukan_nilai(43); 

    ?>
</body>
</html>