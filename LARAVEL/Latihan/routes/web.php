<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [DashboardController::class,'home']);

Route::get('/biodata', [DashboardController::class,'register']);

Route::post('/welcome', [DashboardController::class,'welcome']);

route::get('/table', function (){
    return view('page.table');
});

route::get('/data-table', function (){
    return view('page.data-table');
});


